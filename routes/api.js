var express = require('express');
var router = express.Router();
var Twit = require('twit');
var config = require('../config');

// New Twit module using my config file for oAuth
var twitter = new Twit(config.twitter);

var TWEET_COUNT = 5;
var USER_TIMELINE_URL = 'statuses/user_timeline';
var SEARCH_TWEETS_URL = 'search/tweets';
var OEMBED_URL = 'statuses/oembed';
var MAX_WIDTH = '300';

router.get('/user_timeline/:user', function(req, res, next) {

	var oEmbedTweets = [], tweets = [],

	params = {
		screen_name: req.params.user,
		count: TWEET_COUNT
	}

	// the max_id is passed in via a query string param
	if(req.query.max_id) {
	  params.max_id = req.query.max_id;
	}

	// request data
	twitter.get(USER_TIMELINE_URL, params, function (err, data, resp) {

		tweets = data;
    var i = 0, len = tweets.length;

    for(i; i < len; i++) {
      getOEmbed(tweets[i]);
    }

	});

  function getOEmbed (tweet) {

    // oEmbed request params
    var params = {
      "id": tweet.id_str,
      "maxwidth": MAX_WIDTH,
      "hide_thread": true,
      "omit_script": true
    };

    // request data
    twitter.get(OEMBED_URL, params, function (err, data, resp) {
      tweet.oEmbed = data;
      oEmbedTweets.push(tweet);

      // Get HTML Version of our tweets
      if (oEmbedTweets.length == tweets.length) {
        res.setHeader('Content-Type', 'application/json');
        res.send(oEmbedTweets);
      }
    });
  }

});


router.get('/search/:query', function(req, res, next) {

	var oEmbedTweets = [], tweets = [],

	params = {
		q: req.params.query,
		count: 6
	}

	// the max_id is passed in via a query string param
	if(req.query.max_id) {
	  params.max_id = req.query.max_id;
	}

	// request data
	twitter.get(SEARCH_TWEETS_URL, params, function (err, data, resp) {

		tweets = data.statuses;

    	var i = 0, len = tweets.length;
	    for(i; i < len; i++) {
	      getOEmbed(tweets[i]);
	    }

	});

	  function getOEmbed (tweet) {

    // oEmbed request params
    var params = {
      "id": tweet.id_str,
      "maxwidth": MAX_WIDTH,
      "hide_thread": true,
      "omit_script": true
    };

    // request data
    twitter.get(OEMBED_URL, params, function (err, data, resp) {
      tweet.oEmbed = data;
      oEmbedTweets.push(tweet);

      // Get HTML Version of our tweets
      if (oEmbedTweets.length == tweets.length) {
        res.setHeader('Content-Type', 'application/json');
        res.send(oEmbedTweets);
      }
    });
  }

});

module.exports = router;
