// server.js

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var path 	   = require('path');
var index 	   = require('./routes/index');
var search 	   = require('./routes/search');
var api        = require('./routes/api');
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// serve static assets from the public directory
app.use(express.static(path.join(__dirname, 'public')));

// look for view html in the views directory
app.set('views', path.join(__dirname, 'public/views'));

// use ejs to render
app.set('view engine', 'ejs');

var port = process.env.PORT || 9090;        // set our port

// REGISTER OUR ROUTES
app.use('/', index);
app.use('/api', api);
app.use('/search', search);

// START THE SERVER
app.listen(port);
console.log('Open http://localhost:' + port + ' in your browser.');
