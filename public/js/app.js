angular.module('TwitterApp', ['ngRoute', 'IndexCtrl', 'SearchCtrl'])
.directive('myRepeatDirective', function() {
  return function(scope, element, attrs) {
    if (scope.$last){
    	$('.userInfo').removeClass('hide');
    	$('.loader').addClass('hide');
    	$('.search-list').removeClass('hide');
      $("#owl-example").owlCarousel({
      	items: 1,
      	singleItem: true,
      	navigation: true,
      	navigationText: ["Previous Tweet","Next Tweet"]
      });
    }
  };
});